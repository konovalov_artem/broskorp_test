import broscorp.test.konovalov.calculators.TotalPriceCalculator;
import broscorp.test.konovalov.cart.Cart;
import broscorp.test.konovalov.coupons.CategoryCoupon;
import broscorp.test.konovalov.coupons.Coupon;
import broscorp.test.konovalov.coupons.ForAfterNItemCoupon;
import broscorp.test.konovalov.items.Category;
import broscorp.test.konovalov.items.Item;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        Cart cart = new Cart();

        cart.addItem(new Item(1, "I1", BigDecimal.valueOf(100.0), Category.CAT_1));
        cart.addItem(new Item(2, "I2", BigDecimal.valueOf(200.0), Category.CAT_2));
        cart.addItem(new Item(3, "I3", BigDecimal.valueOf(300.0), Category.CAT_3));

        Coupon coupon1 = new CategoryCoupon(10, Category.CAT_1);
        Coupon coupon2 = new CategoryCoupon(BigDecimal.valueOf(50), Category.CAT_2);
        Coupon coupon3 = new ForAfterNItemCoupon(BigDecimal.valueOf(100), 1);

        cart.addCoupon(coupon1);
        cart.addCoupon(coupon2);
        cart.addCoupon(coupon3);

        TotalPriceCalculator calculator = new TotalPriceCalculator();
        BigDecimal result = calculator.calculate(cart);
        System.out.println(result);
        System.out.println(cart.getItems());
    }
}
