package broscorp.test.konovalov.coupons;

import broscorp.test.konovalov.items.Item;

import java.math.BigDecimal;
import java.util.List;

public class ForAfterNItemCoupon extends Coupon {

    private final int forAllAfter;

    public ForAfterNItemCoupon(float salePercentages, int forAllAfter) {
        super(salePercentages);
        this.forAllAfter = forAllAfter;
    }

    public ForAfterNItemCoupon(BigDecimal saleAmount, int forAllAfter) {
        super(saleAmount);
        this.forAllAfter = forAllAfter;
    }

    @Override
    public void apply(List<Item> items) {
        items.stream().skip(forAllAfter).forEach(this::resetPrice);
    }
}
