package broscorp.test.konovalov.coupons;

import broscorp.test.konovalov.items.Item;

import java.math.BigDecimal;
import java.util.List;

public abstract class Coupon {

    protected float salePercentages;
    protected BigDecimal saleAmount;
    protected boolean byPercentages;

    public Coupon(float salePercentages) {
        this.salePercentages = salePercentages;
        this.byPercentages = true;
    }

    public Coupon(BigDecimal saleAmount) {
        this.saleAmount = saleAmount;
        this.byPercentages = false;
    }

    public abstract void apply(List<Item> items);

    protected void resetPrice(Item item) {
        BigDecimal price = item.getPrice();
        if (byPercentages) {
            item.setPrice(price.subtract(price.multiply(BigDecimal.valueOf(salePercentages / 100.0))));
        } else {
            item.setPrice(price.subtract(saleAmount));
        }
    }
}
