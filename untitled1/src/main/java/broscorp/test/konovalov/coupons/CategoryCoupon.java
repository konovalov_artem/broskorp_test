package broscorp.test.konovalov.coupons;

import broscorp.test.konovalov.items.Category;
import broscorp.test.konovalov.items.Item;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoryCoupon extends Coupon {

    private final List<Category> categories = new ArrayList<>(Category.values().length);

    public CategoryCoupon(float salePercentages, Category... categories) {
        super(salePercentages);
        this.categories.addAll(Arrays.asList(categories));
    }

    public CategoryCoupon(BigDecimal saleAmount, Category... categories) {
        super(saleAmount);
        this.categories.addAll(Arrays.asList(categories));
    }

    @Override
    public void apply(List<Item> items) {
        for (Item item : items) {
            if (categories.contains(item.getCategory())) {
                resetPrice(item);
            }
        }
    }
}
