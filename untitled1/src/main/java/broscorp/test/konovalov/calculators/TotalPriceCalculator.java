package broscorp.test.konovalov.calculators;

import broscorp.test.konovalov.cart.Cart;
import broscorp.test.konovalov.coupons.Coupon;
import broscorp.test.konovalov.items.Item;

import java.math.BigDecimal;
import java.util.List;

public class TotalPriceCalculator {

    public BigDecimal calculate(Cart cart) {
        List<Item> items = cart.getItems();
        List<Coupon> coupons = cart.getCoupons();

        coupons.forEach(coupon -> coupon.apply(items));

        return items.stream().map(Item::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
