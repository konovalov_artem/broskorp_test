package broscorp.test.konovalov.cart;

import broscorp.test.konovalov.coupons.Coupon;
import broscorp.test.konovalov.items.Item;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Cart {

    private final List<Item> items = new LinkedList<>();
    private final List<Coupon> coupons = new ArrayList<>();

    public void addItem(Item item) {
        items.add(item);
    }

    public void addCoupon(Coupon coupon) {
        coupons.add(coupon);
    }

    public List<Item> getItems() {
        return items;
    }

    public List<Coupon> getCoupons() {
        return coupons;
    }
}
